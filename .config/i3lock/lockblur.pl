#!/usr/bin/env perl

use strict;
use warnings;
use Image::Magick;

my $Base_DIR = "/tmp/Lock";
my $lock = "/home/pheonix/.config/i3lock/lock2.png";

sub checks {
	if(! -d $Base_DIR){ 
		unless(mkdir $Base_DIR) {
        	warn "Unable to create $Base_DIR\n";
        	return 0;        	
    	}
	}
	return 1;
}

sub montage {
	my $image = Image::Magick->new;
	$image->Read($_[0]);
	$image->Read($_[1]);

	my $mon=$image->Montage(geometry=>'+0+0');
	$mon->Set(quality=>100);
	$mon->Write($_[2]);
	
	undef $image;
	undef $mon;
}

sub crop {
	my $image = Image::Magick->new;
	$image->Read($_[0]);	
	$image->Crop(geometry=>$_[1]);	
	$image->Set(quality=>100);
	$image->Write($_[2]);
	
	undef $image;
}

sub blur {
	my $image = Image::Magick->new;
	$image->Read($_[0]);

	$image->Blur("0x3");
	$image->Set(quality=>100);
	$image->Write($_[0]);
		
	undef $image;
}

sub add_lock {
	my $image = Image::Magick->new;
	$image->Read($_[0]);

	my $logo = Image::Magick->new;
	$logo->Read("$_[1]");

	$logo->Evaluate(
  		operator => 'Multiply',
  		value    => 0.6,
  		channel  => 'Alpha',
	);

	$image->Composite(
  		image    => $logo,
  		gravity  => 'Center',
	);
	$image->Set(quality=>100);
	$image->Write($_[0]);
	
	undef $image;
	undef $logo;
}

sub clean {
	foreach my $file ( @_ ) {
		unless(unlink $file) {
        	warn "Could not unlink $file: $!";
    	}
	}
}

sub screenlock {
	system qq{i3lock -e -u -i $_[0]};
	system qq{xset dpms force off};	
}

sub run {
	if(checks) {
		my $mainscreen="$Base_DIR/screen.png";
		my $basescreen="$Base_DIR/IMG.png";
		my $wscreen1="$Base_DIR/IMG1.png";
		my $wscreen2="$Base_DIR/IMG2.png";

		system qq{DISPLAY=:0 scrot -q 100 -e 'mv \$f $basescreen'};
		
		blur($basescreen);

		crop($basescreen, '1920x1080+0+0', $wscreen1);
		crop($basescreen, '1920x1080+1920+0', $wscreen2);
		
		add_lock($wscreen1, $lock);
		add_lock($wscreen2, $lock);	
		
		montage($wscreen1, $wscreen2, $mainscreen);
		
		clean(
		$basescreen, 
		$wscreen1, 
		$wscreen2
		);

		screenlock($mainscreen);
	}
}

run;
exit;