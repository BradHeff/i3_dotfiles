#!/usr/bin/env perl

use strict;
use warnings;
use Image::Magick;
use File::Copy;

#Base Images
my $lock="/home/pheonix/.config/i3lock/lock2.png";
my $bwall="/home/pheonix/Pictures/Transister.jpg";

#Temps
my $wall="/tmp/Transister_Blur.jpg";
my $Base_Dir="/tmp";

sub add_blur {
	my $image = Image::Magick->new;
	$image->Read($_[0]);
	$image->Blur("0x5");
	$image->Set(quality=>100);
	$image->Write($_[1]);

	undef $image;
}

sub add_lock {
	my $image = Image::Magick->new;
	$image->Read($_[0]);

	my $logo = Image::Magick->new;
	$logo->Read($_[1]);

	$logo->Evaluate(
  		operator => 'Multiply',
  		value    => 0.6,
  		channel  => 'Alpha',
	);

	$image->Composite(
  		image    => $logo,
  		gravity  => 'Center',
	);
	$image->Set(quality=>100);
	$image->Write($_[2]);
	
	undef $image;
	undef $logo;
}

sub montage {
	my $image = Image::Magick->new;
	$image->Read($_[0]);
	$image->Read($_[1]);

	my $mon=$image->Montage(geometry=>'+0+0');
	$mon->Set(quality=>100);
	$mon->Write($_[2]);
	
	undef $image;
	undef $mon;
}

sub check_constants {
	if (! -e $lock) {
		warn "No $lock";
		return 0;
	}
	if (! -e $bwall) {		
		warn "no $bwall";
		return 0;
	}
	if (! -d $Base_Dir) {
		warn "no $Base_Dir";
		return 0;
	}	
	return 1;
}

if(check_constants) {
	my $mainscreen = "$Base_Dir/screen.png";
	my $wscreen1 = "$Base_Dir/screen1.png";
	my $wscreen2 = "$Base_Dir/screen2.png";
	if (! -e $mainscreen) {
		if (! -e $wscreen1) {
			add_blur($bwall, $wall);
			add_lock($wall, $lock, $wscreen1);
		}
		if (! -e $wscreen2) {
			copy($wscreen1,$wscreen2) or die "Copy failed: $!";
		}
		montage($wscreen1, $wscreen2, $mainscreen);
	}
	system qq{i3lock -e -u -i $mainscreen};
	system qq{xset dpms force off};
}

exit;
