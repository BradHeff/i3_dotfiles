# My Gentoo Dotfiles

The window manager im using is a i3-gaps fork known as [gaps-next](https://github.com/max-lv/i3)

GTK THEME USED
----
The Gtk theme used in the screenshots is Arc-Orange-Theme i found on Github by [eti0](https://github.com/eti0)

[Arc-Theme-Orange](https://github.com/eti0/arc-theme-orange)


ICON THEME USED
----
The Icon theme used in the screenshots is Paper-Icon-Theme i found on Github by [snwh](https://github.com/snwh)

[Paper-Icon-Theme](https://github.com/snwh/paper-icon-theme)


QT COLOR SCHEME USED
----
The color scheme used for qt programs (pcmanfm-qt shown in screenshots) is a customized default darker.conf, i customized it to suit the Arc-Orange-Dark gtk theme.

[darker.conf](https://gitlab.com/BradHeff/i3_dotfiles/tree/master/.config/qt5ct)


FONTS USED
----

Screenshots have been updated with the latest git push to show changes made.

### I3

The font used in my I3 Config you can see in Screenshots on i3blocks is **Terminus (TTF)**

### Conky

The font used in my _.conkyrc_ you can see in Screenshots is **San Francisco Text**

### URXVT

The font used in my _.Xresources_ you can see in Screenshots **Iosevka**


EDIT CONFIG ALIASES
-----

### I3

To edit your i3 config file is as simple as running the following command.

    e3
    
this will execute the following command

    vim ~/.config/i3/config

### Conky

To edit your ~/.conkyrc file is as simple running the following command

    eC

This will execute the following command

    vim ~/.conkyrc

To edit your ~/.conkyrc2 file is as simple running the following command

    eCC

This will execute the following command

    vim ~/.conkyrc2

### BASH

To edit your .bashrc file is as simple as running the following command.

    eZ

This will execute the following command

    vim ~/.bashrc


UPDATE BASH
---

To refresh or update your configured .bashrc file so your current terminal session uses your changes just run this command.

    sZ

This will execute the following command

    source ~/.bashrc


And there are much more. just look in the .bashrc file and see the aliases, there are aliases for git, emerge, copy and remove.


SCREENSHOTS
----

![](Screenshot/screenshot.png)

![](Screenshot/screenshot2.png)

![](Screenshot/screenshot3.png)

![](Screenshot/screenshot4.png)

![](Screenshot/screenshot5.png)


TODO
----

* Nothing left to do
