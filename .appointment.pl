#!/usr/bin/env perl

use strict;
use warnings;
use File::Find;
use File::Basename;
use Scalar::Util qw(looks_like_number);
use POSIX;

my $path = "/home/pheonix/Calendar";
my @files;
my $sysdate = POSIX::strftime("%d %b %Y", localtime());
my $systime = POSIX::strftime("%H:%M", localtime());

sub get_files {
	find(\&wanted, $path);

	sub wanted {
		push @files, $File::Find::name;
		return;
	}
	return @files;
}

sub get_filename {
	my @unsorted;
	foreach my $name (@_) {
		$name = basename($name);
		$name =~ s/^xc//;

		if (looks_like_number(substr $name, 0, length ($name) -7)) {
			push @unsorted, $name;		
		}
	}
	return sort { ($a =~ s/\D.*$//r) <=> ($b =~ s/\D.*$//r) } @unsorted;
}

sub get_date {
	my $year = substr $_[0], length ($_[0]) -4, 4;
	my $month = substr $_[0], length ($_[0]) -7, 3;
	my $day = substr $_[0], 0, length ($_[0]) -7;

	return $day . " " .$month . " " . $year;
}

sub get_appointment {
	open (my $lines, $path . "/xc" . $_[0]) or die "Could not open $path/xc" . $_[0] . ": $!";
	return <$lines>;
	close $lines;
}

sub clear_appointments {
	my $file = shift;
	unlink($file);	
}

sub check_settings {
	if(-e "$path/settings.ini") {
		open (my $lines, "$path/settings.ini") or warn "Could not open $path/settings.ini: $!";		
		if(<$lines> =~ /enable/){
			close $lines;
			return 1;
		}
	}
	return 0;
}

sub check_time {
	if(int(substr $systime, 0, 2) > 06 && int(substr $systime, 0, 2) < 15) {
		if(check_settings) {
			return 0;
		} else {
			my $file = $path . "/settings.ini";

		    open (my $fh, ">> $file") or do { warn "Could not open $file: $!"; exit; };			
			print $fh "enabled";
		    close $fh;
		    return 1;
		}
	} else {
		if(-e "$path/settings.ini") {
			clear_appointments($path . "/settings.ini");
		}
		return 0;
	}	
}

foreach my $names (get_filename(get_files)) {
	my $date = get_date($names);	
	
	if(int(substr $names, 0, length ($names) -7) < int(substr $sysdate, 0, length($sysdate) -9)) {
		clear_appointments($path . "/xc" . $names);		
	}

	my @appointment = get_appointment($names);

	if($sysdate eq $date) {		
		$date = "TODAY";
		if(check_time) {
			my $cmd = "notify-send -u critical --app-name=\"Appointment\" \"@appointment\" \"$date\"";
			system qq{$cmd};
		}
		$date = '${color4}' . $date . '${color}';
	}	
	print '${font San Francisco Text:size=11:style=Bold}', $date, '${font}', "\n", @appointment, "\n", '${hr 1}', "\n";
}